﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RepairUnit : NetworkBehaviour
{
    public float rotationSpeed;
    public int healthRegained;
    public Transform spawnPoint_1;
    public Transform spawnPoint_2;
    public Transform spawnPoint_3;
    public Transform OutOfBounds;

    [SyncVar(hook = "CmdRespawnRepairUnit")]
    public Vector3 repairUnitPos;

    void Start()
    {
        CmdRespawnRepairUnit(repairUnitPos);
    }
    
    void Update()
    {
        this.transform.Rotate(Vector3.forward * rotationSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "RB")
        {
            var health = other.GetComponentInParent<HP>();
            health.TakeDamage(-healthRegained);
            CmdRespawnRepairUnit(repairUnitPos);
        }
    }

    [Command]
    public void CmdRespawnRepairUnit(Vector3 unitPos)
    {
        this.transform.position = OutOfBounds.position;
        
            int rand = Random.Range(1, 4);

            if (rand == 1)
            {
                this.transform.position = spawnPoint_1.transform.position;
            }
            if (rand == 2)
            {
                this.transform.position = spawnPoint_2.transform.position;
            }
            if (rand == 3)
            {
                this.transform.position = spawnPoint_3.transform.position;
            }

            unitPos = this.transform.position;             
    }
}
