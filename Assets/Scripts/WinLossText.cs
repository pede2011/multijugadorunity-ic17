﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class WinLossText : NetworkBehaviour
{
    public GameObject textPrefab;
    private GameObject[] players;

    private int gameOver = 0;

    void Start()
    {
        textPrefab.GetComponentInChildren<Text>().text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver>=2)
        {
            return;
        }

        players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetComponent<HP>().ammountOfLives <= 0)
            {
                if (isLocalPlayer)
                {
                    if (this.GetComponent<HP>().ammountOfLives <= 0)
                    {
                        Debug.Log("YOU LOST M8");                        
                        Instantiate(textPrefab, Vector3.zero, textPrefab.transform.rotation);
                        textPrefab.GetComponentInChildren<Text>().text = "LOSER";
                        gameOver++;
                    }

                    else
                    {
                        Debug.Log("YOU WON YEAH");                        
                        Instantiate(textPrefab, Vector3.zero, textPrefab.transform.rotation);
                        textPrefab.GetComponentInChildren<Text>().text = "WINNER";
                        gameOver++;
                    }
                }              
            }
        }
    }
}