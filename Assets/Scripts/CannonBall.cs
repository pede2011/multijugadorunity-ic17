﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    public float bulletSpeed;
    public int bulletDMG;

    public GameObject explosionPrefab;
    public float explosionRadius;// explosion radius
    public float explosionForce;// explosion forse
    public int explosionDMG;
    private bool beenHitWithExplosion=false;

    void Start ()
    {
        this.GetComponent<Rigidbody>().velocity = this.transform.forward * bulletSpeed;
        Destroy(this.gameObject, 2.0f);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Repair")
        {
            return;
        }

        beenHitWithExplosion = false;

        var hit = collision.gameObject;
        var health = hit.GetComponent<HP>();
        if (health != null)
        {
            health.TakeDamage(bulletDMG);
        }

        ContactPoint contact = collision.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point;
        Instantiate(explosionPrefab, pos, rot);
        this.gameObject.SetActive(false);

        /*Collider[] hitColliders = Physics.OverlapSphere(transform.position, explosionRadius);// create explosion
        for (int i = 0; i < hitColliders.Length; i++)
        {
            var healthExplosion = hitColliders[i].GetComponent<HP>();

            if (health != null && beenHitWithExplosion==false)
            {
                healthExplosion.TakeDamage(explosionDMG);
                beenHitWithExplosion = true;
            }

            if (hitColliders[i].CompareTag("CanDestroy"))// if tag CanBeRigidbody
            {
                if (!hitColliders[i].GetComponent<Rigidbody>())
                {
                    hitColliders[i].gameObject.AddComponent<Rigidbody>();
                }
                hitColliders[i].GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, explosionRadius, 0.0F); // push game object
            }

        }*/
        //Destroy(explosionPrefab, 0.2f);// destroy explosion
    }
}
