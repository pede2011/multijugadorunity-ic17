﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    public GameObject explosionPrefab;
    public GameObject firePrefab;
    public float explosionDuration;

    private Color playerColor;

    //private float movedownX = 0.0f;
    //private float movedownZ = 0.0f;
    //public float sensitivity;
    public float turretSpeed;

    public float speedForward;
    public float speedRotate;

    public float fireRate;
    private float initFireRate;
    private float timer;

    public GameObject OutOfBounds;

    private Transform tempChild;

    [SyncVar]
    public bool isAlive;    

    void Start()
    {
        isAlive = true;

        OutOfBounds = GameObject.FindGameObjectWithTag("OutOfBounds");

        if (!isLocalPlayer)
        {
            playerColor = Color.blue;
        }
        else
        {
            playerColor = Color.red;
        }

        tempChild = this.transform.Find("Tower");

        initFireRate = fireRate;
    }

    void Update()
    {
        if (!isLocalPlayer || isAlive == false)
        {
            return;
        }
        
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * speedForward;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * speedRotate;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        switch (this.GetComponent<HP>().currentHealth)
        {
            case 100:
                fireRate = initFireRate;
                break;

            case 75:
                fireRate = (initFireRate - initFireRate / 10);
                break;

            case 50:
                fireRate = (initFireRate - initFireRate / 10);
                break;

            case 25:
                fireRate = (initFireRate - initFireRate / 10);
                break;
        }

        timer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && fireRate<=timer)
        {
            CmdFire();
            timer = 0.0f;
        }

        if (Input.GetKey("e"))
        {
            tempChild.transform.Rotate(Vector3.forward * turretSpeed);
            
        }
        if(Input.GetKey("q"))
        {
            tempChild.transform.Rotate(Vector3.back * turretSpeed);
        }

        if (this.gameObject.GetComponent<HP>().ammountOfLives<=0)
        {
            this.transform.position = OutOfBounds.transform.position;
            CmdExplosion();
            isAlive = false;
        }

        //movedownX = 0.0f;

        /* movedownZ += Input.GetAxis("Mouse Y") * sensitivity;
         if (Input.GetAxis("Mouse Y") != 0)
         {
             tempChild.transform.Rotate(Vector3.back * movedownZ);
         }
         movedownZ = 0.0f;*/
    }

    public override void OnStartLocalPlayer()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var tempChild = transform.GetChild(i);

            if (tempChild.name=="Healthbar Canvas")
            {
                return;
            }
            else
            {
                tempChild.GetComponent<MeshRenderer>().material.color = playerColor;
            }
        }
    }

    [Command]
    void CmdFire()
    {        
        var bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

        NetworkServer.Spawn(bullet);

        var explosion = (GameObject)Instantiate(explosionPrefab, bulletSpawn.position, bulletSpawn.rotation);

        NetworkServer.Spawn(explosion);

        Destroy(explosion, explosionDuration);
    }

    [Command]
    void CmdExplosion()
    {
        var fire = (GameObject)Instantiate(firePrefab, this.transform.position, this.transform.rotation);

        NetworkServer.Spawn(fire);
    }
}